package com.mojay.ticketapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.sql.Date;
import java.sql.Time;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "tickets")
public class Ticket {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "userId")
    @NonNull
    private String userId;

    @Column(name = "firstName")
    @NonNull
    private String firstName;

    @Column(name = "lastName")
    @NonNull
    private String lastName;

    @Column(name = "name")
    @NonNull
    private String name;

    @Column(name = "date")
    @NonNull
    private Date date;

    @Column(name = "time")
    @NonNull
    private Time time;

    @Column(name = "venueName")
    @NonNull
    private String venueName;
}
