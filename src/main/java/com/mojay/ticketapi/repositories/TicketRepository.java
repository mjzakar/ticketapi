package com.mojay.ticketapi.repositories;

import com.mojay.ticketapi.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TicketRepository extends JpaRepository<Ticket, String> {
    Optional<List<Ticket>> findByUserId(String userId);
}