package com.mojay.ticketapi.rest;

import com.mojay.ticketapi.models.Ticket;
import com.mojay.ticketapi.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tickets")
public class TicketREST {

    private final TicketRepository ticketRepository;

    @Autowired
    public TicketREST(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @PostMapping
    public ResponseEntity<Ticket> addTicket(@RequestBody Ticket ticket) {
        Ticket addedTicket = ticketRepository.save(ticket);
        return new ResponseEntity<>(addedTicket, HttpStatus.CREATED);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<Ticket>> getAllTicketsForUser(@PathVariable String userId) {
        Optional<List<Ticket>> userTickets = ticketRepository.findByUserId(userId);
        return userTickets.map(tickets -> new ResponseEntity<>(tickets, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @GetMapping("/{ticketId}")
    public ResponseEntity<Ticket> getTicketById(@PathVariable String ticketId) {
        return ticketRepository.findById(ticketId)
                .map(ticket -> new ResponseEntity<>(ticket, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
